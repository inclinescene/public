# Incline - a demogroup

We released our first demo at atparty 2016 - <https://demozoo.org/productions/158999/>

More on 
[Demozoo](https://demozoo.org/groups/65997/) or [Pouet](http://www.pouet.net/groups.php?which=12740)!

## Members are:

 * cxw (code)
 * phrank (code)
 * makro (music)

## Stay tuned for more!

